﻿using System;
using System.Collections.Generic;
using FoxmindTestApp.Infrastructure.Utils.FileProcessing;
using Xunit;

namespace FoxmindTest.Infrastructure.Utils
{
    public class FileProcessingUtilsTests
    {
        private static readonly List<string> LinesWithCorrectValues = new List<string>
        {
            "1,1,1,1,1,1",
            "2,2,2,2,2,2",
            "3,3,3,3,3,3",
            "0,0,0,0,0,0"
        };

        private static readonly List<string> LinesWithDecimalNumbers = new List<string>
        {
            "8.9,6.0,5.5,1.7,4.5,4.9",
            "2,3,2",
            "3,3,3,3,3,3",
            "0,3,0,0"
        };

        private static readonly List<string> LinesWithRejectedValues = new List<string>
        {
            "8.9,6.0,5.5,1.7,4.5,4.9",
            "2,bbb,2",
            "3,3,3,3,3,3",
            "0,kkk,0,0"
        };

        [Fact]
        public void FindStrWithMaxElement_RightArray_SuccessResult()
        {
            var expectedResult = new KeyValuePair<int, double>(3, 18);

            Assert.Equal(expectedResult, 
                FileProcessUtils.GetSumElementsEachString(LinesWithCorrectValues));
        }

        [Fact]
        public void FindStrWithMaxElement_WithRejectedStrings_SuccessResult()
        {
            var expectedResult = new KeyValuePair<int, double>(1, 31.50);

            Assert.Equal(expectedResult, 
                FileProcessUtils.GetSumElementsEachString(LinesWithRejectedValues));
        }

        [Fact]
        public void FindStrWithMaxElement_WithDecimalNumbers_SuccessResult()
        {
            var expectedResult = new KeyValuePair<int, double>(1, 31.50);

            Assert.Equal(expectedResult, FileProcessUtils
                .GetSumElementsEachString(LinesWithDecimalNumbers));
        }

        [Fact]
        public void FindStrWithMaxElement_EmptyArray_EmptyObject()
        {
            Assert.Equal(new KeyValuePair<int, double>(), 
                FileProcessUtils.GetSumElementsEachString(new List<string>()));
        }

        [Fact]
        public void FindStrWithMaxElement_NullValue_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => FileProcessUtils.GetSumElementsEachString(null));
        }

        [Fact]
        public void GetRejectedStrings_NullValue_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => FileProcessUtils.GetRejectedString(null));
        }

        [Fact]
        public void GetRejectedStrings_ArrayWithRejectsStrings_SuccessResult()
        {

            var expectedResult = new List<int>{2,4};

            Assert.Equal(expectedResult, FileProcessUtils.GetRejectedString(LinesWithRejectedValues));
        }

        [Fact]
        public void GetRejectedStrings_ArrayWithoutRejectsStrings_SuccessResult()
        {
            Assert.Equal(new List<int>(), FileProcessUtils.GetRejectedString(LinesWithDecimalNumbers));
        }
    }
}
