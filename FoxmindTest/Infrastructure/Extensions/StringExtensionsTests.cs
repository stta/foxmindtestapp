﻿using FoxmindTestApp.Infrastructure.Extensions;
using Xunit;

namespace FoxmindTest.Infrastructure.Extensions
{
    public class StringExtensionsTests
    {
        [Fact]
        public void Text_IsNumeric_SuccessResult()
        {
            Assert.True("123".IsNumeric());
        }

        [Fact]
        public void Text_IsNumericAndDecimal_SuccessResult()
        {
            Assert.True("123.12".IsNumeric());
        }

        [Fact]
        public void Text_IsNumericAndDecimalIncorrect_UnsuccessfulResult()
        {
            Assert.False("123.12.12".IsNumeric());
        }

        [Fact]
        public void Text_IsNotNumeric_UnsuccessfulResult()
        {
            Assert.False("asd".IsNumeric());
        }

        [Fact]
        public void Text_IsNotNumericAndNotNumeric_UnsuccessfulResult()
        {
            Assert.False("asd123".IsNumeric());
        }

        [Fact]
        public void Text_IsNull_NonSuccessfulResult()
        {
            string testStr = null;
            Assert.False(testStr.IsNumeric());
        }
    }
}
