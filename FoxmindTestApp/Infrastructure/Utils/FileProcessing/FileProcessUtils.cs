﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FoxmindTestApp.Infrastructure.Extensions;

namespace FoxmindTestApp.Infrastructure.Utils.FileProcessing
{
    public static class FileProcessUtils
    {
        public static KeyValuePair<int, double> GetSumElementsEachString(IReadOnlyList<string> fileLines)
        {
            var sumElementsEachString = new Dictionary<int, double>();

            if (fileLines == null)
            {
                throw new ArgumentException("fileLines shouldn't be null");
            }

            for (var i = 0; i < fileLines.Count; i++)
            {
                var elements = fileLines[i]
                    .Split(",")
                    .ToList();

                var trimmedElements = elements
                    .Select(item => item.Trim())
                    .ToList();

                var isRejectedString = trimmedElements.Any(item => !item.IsNumeric());
                if (isRejectedString)
                {
                    continue;
                }

                var sum = trimmedElements
                    .Sum(item => double
                        .Parse(item, NumberStyles.Any, new CultureInfo("en-US")));

                sumElementsEachString.Add(i + 1, sum);
            }

            return !sumElementsEachString.Any() 
                ? new KeyValuePair<int, double>() 
                : sumElementsEachString
                    .Aggregate((x, y) => x.Value > y.Value ? x : y);
        }

        public static IEnumerable<int> GetRejectedString(IReadOnlyList<string> fileLines)
        {
            var rejectedStringNumbers = new List<int>();

            if (fileLines == null)
            {
                throw new ArgumentException("fileLines shouldn't be null");
            }

            for (var i = 0; i < fileLines.Count; i++)
            {
                var stringElements = fileLines[i].Split(",");

                var trimmedElements = stringElements
                    .Select(item => item.Trim());

                var isRejectedString = trimmedElements
                    .Any(item => !item.IsNumeric());
                if (!isRejectedString)
                {
                    continue;
                }
                rejectedStringNumbers.Add(i + 1);
            }

            return rejectedStringNumbers;
        }
    }
}
