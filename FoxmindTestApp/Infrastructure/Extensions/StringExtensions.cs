﻿using System.Globalization;

namespace FoxmindTestApp.Infrastructure.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNumeric(this string text) => decimal
            .TryParse(
                text,
                NumberStyles.Any,
                new CultureInfo("en-US"),
                out _);
    }
}
