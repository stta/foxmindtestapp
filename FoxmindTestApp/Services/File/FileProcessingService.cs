﻿using System;
using System.IO;
using System.Threading.Tasks;
using FoxmindTestApp.Infrastructure.Constants;
using FoxmindTestApp.Infrastructure.Utils.FileProcessing;

namespace FoxmindTestApp.Services.File
{
    public class FileProcessingService : IFileProcessingService
    {
        public async Task FindStingWithMaxSumElementsOfFile()
        {
            var filePath = GetFilePath();
            var fileLines = await System.IO.File.ReadAllLinesAsync(filePath);

            var (stringNumber, elementsSum) = FileProcessUtils.GetSumElementsEachString(fileLines);
            Console.WriteLine("---- (Номер строки с максимальной суммой элементов) ----");
            Console.WriteLine($"В строке {stringNumber} сума єлементов составляет {elementsSum} ");

            var rejectedStringNumbers = FileProcessUtils.GetRejectedString(fileLines);
            Console.WriteLine("------------------- (бракованные строки) ---------------------");
            Console.WriteLine($"{string.Join(",", rejectedStringNumbers)}");
        }

        private string GetFilePath()
        {
            Console.Clear();
            Console.WriteLine("------------------- Выберите вариант загрузки документа ---------------------");

            Console.WriteLine("1. Дефолтный тестовый файл");
            Console.WriteLine("2. Ввести путь к файлу для обработки ");

            while (true)
            {
                var variant = Console.ReadLine();

                switch (variant)
                {
                    case "1":
                        return $"{ Directory.GetCurrentDirectory()}{FoxmindTestAppConstants.DefaultFilePath}";
                    case "2":
                        return GetCustomFilePath();
                    default:
                        Console.WriteLine("Данные введены не верно. Введите пожалуйста еще раз.");
                        break;
                }
            }
        }

        private string GetCustomFilePath()
        {
            Console.Clear();
            Console.WriteLine("Введите, пожалуйста путь к файлу");

            while (true)
            {
                var userVariant = Console.ReadLine();

                if (System.IO.File.Exists(userVariant))
                {
                    return userVariant;
                }

                Console.WriteLine("По указанному пути файл не найден. Проверьте, пожалуйста еще раз данный путь к файлу.");
            }
        }
    }
}

