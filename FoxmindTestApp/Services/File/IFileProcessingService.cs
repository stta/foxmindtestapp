﻿using System.Threading.Tasks;

namespace FoxmindTestApp.Services.File
{
    public interface IFileProcessingService
    {
        Task FindStingWithMaxSumElementsOfFile();
    }
}
