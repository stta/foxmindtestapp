﻿using System.Threading.Tasks;

namespace FoxmindTestApp.Services
{
    public interface IMainAppService
    {
        Task StartApplication();
    }
}
