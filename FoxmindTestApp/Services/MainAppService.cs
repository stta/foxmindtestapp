﻿using System;
using System.Threading.Tasks;
using FoxmindTestApp.Services.File;

namespace FoxmindTestApp.Services
{
    public class MainAppService : IMainAppService
    {
        private readonly IFileProcessingService _fileProcessingService;

        public MainAppService(IFileProcessingService fileProcessingService)
        {
            _fileProcessingService = fileProcessingService;
        }

        public async Task StartApplication()
        {
            ApplicationMenuStarting();

            while (true)
            {
                await _fileProcessingService.FindStingWithMaxSumElementsOfFile();
                if (!ChooseMenuActionAfterEndOfProcessing())
                {
                    Environment.Exit(0);
                }
            }
        }

        private void ApplicationMenuStarting()
        {
            Console.WriteLine("------------------- Добро пожаловать в тестовое приложение ---------------------");
            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine("-------------------Нажмите клавишу \"Enter\", что бы начать работу--------------");

            Console.ReadLine();
            Console.Clear();
        }

        private bool ChooseMenuActionAfterEndOfProcessing()
        {
            for (var i = 0; i < 15; i++)
            {
                Console.WriteLine("");
            }

            Console.WriteLine("------------------- (Выберите варианты для продолжения работы) ---------------------");

            Console.WriteLine("1. Завершить работу приложения");
            Console.WriteLine("2. Начать работу заново");

            while (true)
            {
                var variant = Console.ReadLine();

                switch (variant)
                {
                    case "1": return false;
                    case "2": return true;
                    default:
                        Console.WriteLine("Поторите пожалуйста Ваш выбор!");
                        break;
                }
            }
        }
    }
}
