﻿using System.Threading.Tasks;
using FoxmindTestApp.Services;
using FoxmindTestApp.Services.File;
using Microsoft.Extensions.DependencyInjection;

namespace FoxmindTestApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddSingleton<IFileProcessingService, FileProcessingService>()
                .AddSingleton<IMainAppService, MainAppService>()
                .BuildServiceProvider();

            var mainApp = serviceProvider.GetService<IMainAppService>();
            await mainApp.StartApplication();
        }
    }
}
